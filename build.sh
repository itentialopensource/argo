#!/bin/bash
# Build & publish all images
PROJECT_NAME="argo"

# Create the build directories.
while IFS=, read -r BUILD_NAME REGISTRY_PATH; do
  mkdir -p builds/"$BUILD_NAME"
  cp Dockerfile.template builds/"$BUILD_NAME"/Dockerfile
  cp -r src/scripts builds/"$BUILD_NAME"
  cp -r src/config builds/"$BUILD_NAME"
  chmod +x builds/"$BUILD_NAME"/scripts/*
  sed -i '' \
    -e "s %%REGISTRY_PATH%% $REGISTRY_PATH g" \
    "builds/$BUILD_NAME/Dockerfile"
done < build_names.csv

# Get the directories of each Dockerfile & create the images.
while read -r DIR; do
  # Create an array of all of the version paths
  VERSIONS=("$(dirname "$DIR")")
  for VERSION_PATH in "${VERSIONS[@]}"; do
    # get the version tag
    VERSION_TAG="$(basename "$VERSION_PATH")"
    # move into the version's directory to build & publish the docker image.
    cd "$VERSION_PATH" || exit 1
    docker build -t itential/"$PROJECT_NAME":"$VERSION_TAG" . || exit 1
    if [ "$1" == "--push" ] || [ "$1" == "-p" ]; then
      docker push itential/"$PROJECT_NAME":"$VERSION_TAG" || exit 1
    fi
    cd ~- || exit 1
    echo -e "\\n\\n\\nDocker image for $VERSION_TAG has been built!\\n\\n\\n"
  done
done < <(find . -name Dockerfile)
