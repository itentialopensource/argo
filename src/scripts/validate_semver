#!/bin/bash
set -e

#
# git credential setup.
#
git_setup

#
# Get semver prefix.
#

# Get branch prefix for semver bump, otherwise default to patch.
VERSION="$(echo "$CI_COMMIT_REF_NAME"|awk -F "/" '{print $1}')"
# Enable case globbing for matching "patch" and "Patch".
shopt -s nocasematch
case "$VERSION" in
  # If PATCH then transform to patch.
  patch|minor|major) SEMVER="$(echo "$VERSION" | awk '{print tolower($0)}')";;
  *) echo "Invalid version prefix: $VERSION" && exit 1;;
esac
# Disable case matching.
shopt -u nocasematch

echo "Proposed semver: $SEMVER"

#
# Validate semver prefix.
#
echo "Merge Request Target Branch: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"

if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == *"release"* ]]; then
  if [[ "$SEMVER" != "patch" ]]; then
    echo -e "\033[0;31mError: ***********************************************************************************"
    echo "Error: Only patches are allowed to merge into release branches."
    echo "Error: Re-evaluate the proposed change, make sure it is a patch, and"
    echo "Eerror: then resubmit a new merge request."
    echo -e "Error: ***********************************************************************************\033[0m"
    exit 1
  else
    echo "Semver prefix is valid."
  fi
fi
