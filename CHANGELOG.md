
## 2.14.1 [07-16-2020]

* Allow security job to fail until lodash fix or other solution.

See merge request itentialopensource/argo!64

---

## 2.14.0 [06-05-2020]

* Replace hyphens with periods in new version.

See merge request itentialopensource/argo!62

---

## 2.13.1 [06-04-2020]

* Change quality-merge job to quality job.

See merge request itentialopensource/argo!63

---

## 2.13.0 [06-03-2020]

* Validate semver branch on first submittal of MR.

See merge request itentialopensource/argo!61

---

## 2.12.5 [04-30-2020]

* Temporarily remove check_vars calls in k8s jobs

See merge request itentialopensource/argo!60

---

## 2.12.4 [04-30-2020]

* Added python 3.6 with poetry v1

See merge request itentialopensource/argo!59

---

## 2.12.3 [04-30-2020]

* Use our own python dind images

Closes #9

See merge request itentialopensource/argo!58

---

## 2.12.2 [04-29-2020]

* Resolve "Scripts to sanity check variables' existence"

See merge request itentialopensource/argo!57

---

## 2.12.1 [04-15-2020]

* Revert the addition of validate_semver job.

See merge request itentialopensource/argo!56

---

## 2.12.0 [04-11-2020]

* Add validate semver script.

See merge request itentialopensource/argo!55

---

## 2.11.2 [04-07-2020]

* rename 'latest' to lts for node templates

See merge request itentialopensource/argo!53

---

## 2.11.1 [03-31-2020]

* Add new python images and templates. Update node images

See merge request itentialopensource/argo!52

---

## 2.11.0 [03-12-2020]

* add prerelease stage to all templates

See merge request itentialopensource/argo!51

---

## 2.10.1 [03-04-2020]

* Test Builds before before deploying

See merge request itentialopensource/argo!50

---

## 2.10.0 [03-03-2020]

* warn about possible conflicts with multiple major branches instead of erroring out completely

See merge request itentialopensource/argo!49

---

## 2.9.0 [03-03-2020]

* warn about possible conflicts with multiple major branches instead of erroring out completely

See merge request itentialopensource/argo!49

---

## 2.8.0 [03-02-2020]

* warn about possible conflicts with multiple major branches instead of erroring out completely

See merge request itentialopensource/argo!49

---

## 2.7.0 [03-02-2020]

* warn about possible conflicts with multiple major branches instead of erroring out completely

See merge request itentialopensource/argo!49

---

## 2.6.10 [02-25-2020]

* redeploy images for argo

See merge request itentialopensource/argo!48

---

## 2.6.9 [02-21-2020]

* redeploy images for argo

See merge request itentialopensource/argo!48

---

## 2.6.8 [02-21-2020]

* redeploy images for argo

See merge request itentialopensource/argo!48

---

## 2.6.7 [09-18-2019]

* install spar instead of using npx

See merge request itentialopensource/argo!45

---

## 2.6.6 [09-18-2019]

* Update VERSION

See merge request itentialopensource/argo!44

---

## 2.6.5 [09-18-2019]

* Update VERSION

See merge request itentialopensource/argo!44

---

## 2.6.3 [08-08-2019]

* :wrench: disable docker tls

See merge request itentialopensource/argo!42

---

## 2.6.2 [08-05-2019]

* Patch/include markdown cli

See merge request itentialopensource/argo!41

---
## 2.6.1 [07-19-2019]
* :wrench: Include project name in version post-back

See merge request itentialopensource/argo!40

---

## 2.6.0 [07-19-2019]
* Update Release Notes Script

See merge request itentialopensource/argo!38

---

## 2.5.7 [07-17-2019]
* Remove opportunity for poetry version and NEW_VERSION to diverge

See merge request itentialopensource/argo!39

---

## 2.5.6 [07-16-2019]
* :wrench: send a timeout error if seconds is equal to the provided timeout.

See merge request itentialopensource/argo!35

---

## 2.5.5 [07-08-2019]
* Better poetry support in prerelease

See merge request itentialopensource/argo!37

---

## 2.5.4 [06-18-2019]
* :gem: Use kustomize

See merge request itentialopensource/argo!32

---

## 2.5.3 [06-10-2019]
* Use the correct CI variables when publishing a poetry project

See merge request itentialopensource/argo!34

---

## 2.5.2 [06-03-2019]
* Bug fixes and performance improvements

See commit 96eebcc

---

## 2.5.1 [06-03-2019]
* Bug fixes and performance improvements

See commit 62191c3

---

## 2.5.0 [06-03-2019]
* Add release version field postback for jira tickets.

See merge request itentialopensource/argo!33

---

## 2.4.0 [05-30-2019]
* Add release version field postback for jira tickets.

See merge request itentialopensource/argo!33

---

## 2.3.4 [05-28-2019]
* :bug: copy everything from scripts to usr/bin

See merge request itentialopensource/argo!31

---

## 2.3.3 [05-25-2019]
* :bug: copy everything from scripts to usr/bin

See merge request itentialopensource/argo!31

---

## 2.3.2 [05-24-2019]
* :bug: copy everything from scripts to usr/bin

See merge request itentialopensource/argo!31

---
