<!---
CHORE ISSUE TEMPLATE:
Issues with the "chore" label track non-business value tasks.

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "chore" label:
https://gitlab.com/itentialopensource/argo/issues

Verify the issue you're about to submit isn't a duplicate.
--->

## Description

<!-- Write a short description of the proposed change. Add pictures and log output if possible. -->

### Related Issues & Merge Requests

<!-- Mention any issue(s) or merge request(s) that this Merge Request closes or is related to. -->

/label ~chore
