# ![Argo](./src/img/argo.png)

Argo is a [CI/CD image available for everyone](https://hub.docker.com/r/itential/argo), used to build, test, and deploy a project.

Itential uses Argo and the [Itential Git Flow](./GIT_FLOW.md) to build, test, and deploy modules. The Argo Docker images provide [various useful commands](#available-commands) that can be used in a CI pipeline to automate building and deploying projects via [semantic versioning](https://semver.org/).

## Why

Itential found that while [Semantic Versioning](https://semver.org/) is well-defined, there are very few full-featured deployment images built around unifying and streamlining CI/CD pipelines. GitLab has done a great job with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/), and many projects use [semantic-release](https://github.com/semantic-release/semantic-release) for handling release notes and package publishing. However, Itential found that while semantic-release *almost* handles everything that we wanted, it doesn't necessarily keep changes granular enough (for example, you can mix multiple merges into master into one release), it requires extra configuration steps to get to an automated release trigger, and it is fairly exclusive to node-module development. Itential wanted to use the same flow for any software project (node, python, java, etc.), while also keeping every single merge into master granular and well-defined without the need for a central release manager to trigger releases. Thus Argo was created as an implementation of the [Itential Git Flow](./GIT_FLOW.md).

## Supported CI/CD Tools

* [GitLab + GitLab CI](https://docs.gitlab.com/ee/ci/)
* [Bitbucket + Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines)

## Getting Started

1. Set up the necessary [environment variables](#environment-variables) in your project's settings. For Bitbucket projects, make sure to enable pipelines in your project's Pipelines Settings first!

1. Add a `.gitlab-ci.yml` or `bitbucket-pipelines.yml` to your new or existing project.

    Examples for both files can be found in this project's [examples directory](./examples).

    Templates for GitLab CI projects can be found in this project's [templates directory](./templates).

1. Commit and push to your project with the new ci file.

That's it! For more information on configuring your ci file, take a look at the official documentation for [gitlab ci](https://docs.gitlab.com/ee/ci/yaml/) or [bitbucket pipelines](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html). Information for Argo's available commands can be found in the [Available Commands](#available-commands) section of this README.

## Environment Variables

To fully utilize all of the available options & tooling in Argo, a few environment variables are required.

### Git Credentials _(required)_

The following git credentials require a user with permissions to write to your project's master branch.

| Variable                    | Example Value               |
| --------------------------- | --------------------------- |
| `ARGO_GIT_EMAIL`            | `example@somewhere.com`     |
| `ARGO_GIT_USERNAME`         | `example-user`              |
| `ARGO_ID_RSA`               | Contents of an id_rsa key. For bitbucket, make sure to include newlines, `\n`. |

### SCM Host Override _(optional)_

SCM host override allows for the use of self hosted versions of github, gitlab, and bitbucket.

| Variable                  | Example Value                    | Description                                                                              |
| ------------------------- | ---------------------------------| ---------------------------------------------------------------------------------------- |
| `ARGO_SCM_HOST_NAME`      | `git@self-hosted-gitlab.com`     | When this variable is not applied then the predefined is based on CI provided variables. |

### Private Docker Registries _(optional)_

If you're using a private Docker registry, you will need the following credentials.

| Variable                        | Example Value                                   | Description                                                                             |
| ------------------------------- | ------------------------------------------------| ----------------------------------------------------------------------------------------|
| `ARGO_DOCKER_REGISTRY_URL`      | `registry.example.com`                          |                                                                                         |
| `ARGO_DOCKER_REGISTRY_USERNAME` | `example.username`                              |                                                                                         |
| `ARGO_DOCKER_REGISTRY_PASSWORD` | `examplepassword`                               |                                                                                         |
| `ARGO_DOCKER_BUILD_ARGS`        | `--build-arg=KEY=VALUE --build-arg=KEY1=VALUE1` | Mutiple args should be passed in with multiple `--build-arg=`. Note the additional `=`. |

### Private npm Registries _(optional)_

For [npm](https://docs.npmjs.com/about-npm/) projects, an `.npmrc` is required for private registry login. An `.npmrc` file can typically be found in your home directory, e.g., `~/.npmrc`. For more information on the `.npmrc` file, take a look at npm's [official documentation on the npmrc](https://docs.npmjs.com/files/npmrc).

| Variable                  | Example Value                          |
| ------------------------- | -------------------------------------- |
| `ARGO_NPMRC`              | Contents of an `.npmrc` file. For Bitbucket, make sure to include newlines, `\n`. |

### Private Python Repositories _(optional)_

For Python projects, Argo currently supports [poetry](https://poetry.eustace.io/) for package management.

| Variable                        | Example Value                                          |
| ------------------------------- | ------------------------------------------------------ |
| `ARGO_POETRY_REGISTRY_URL`      | `https://registry.example.com/repository/examplerepo/` |
| `ARGO_POETRY_REGISTRY_USERNAME` | `exampleuser`                                          |
| `ARGO_POETRY_REGISTRY_PASSWORD` | `examplepass`                                          |
| `ARGO_POETRY_REPOSITORY`        | `examplerepo`                                          |

### Jira Integration _(optional)_

Projects using Jira may use the following environment variables to auto-populate release notes as well as post version information back to a Jira issue when a deployment has completed.

| Variable                          | Example Value                  | Description |
| --------------------------------- | ------------------------------ | ----------- |
| `ARGO_JIRA_URL`                   | `http://example.atlassian.net` | URL to your team's Jira server. |
| `ARGO_JIRA_AUTH_KEY`              | `asdfqwert1234thisisanexample` | [Jira Basic Authentication Key](https://developer.atlassian.com/server/jira/platform/basic-authentication/) |
| `ARGO_JIRA_RELEASE_NOTE_FIELD`    | `customfield_10400`            | The custom Jira field used to fill out changelogs. |
| `ARGO_JIRA_RELEASE_VERSION_FIELD` | `customfield_11904`            | The custom Jira field used to show the released version. |
| `ARGO_NPM_MODULE_REGISTRY_PATH`   | `https://registry.example.com/service/rest/repository/browse/example/%40examplescope` | Argo can create comments in Jira issues containing the released version of your project. To do this, add the URL to the deployed project into this variable. |

### Documentation Transport _(optional)_

Argo can handle transporting documentation from your project into a single central repository to organize project documentation. If you want to transport documentation to another central location, the following variables are required.

| Variable                 | Example Value                     | Description              |
| ------------------------ | ----------------------------------| ------------------------ |
| `ARGO_DOCS_PROJECT_PATH` | `itential/devops/project-docs`    | Path to docs repository. |
| `ARGO_DOCS_PROJECT_NAME` | `project-docs`                    | Docs repository name.    |

### Kubernetes Deployment _(optional)_

Argo offers the ability to deploy to a Google Cloud Platform (GCP) Google Kubernetes Engine (GKE). Argo assumes kubernetes manifests have been created and added to a `manifests/` directory in the project's root.

If you're new to GCP and/or GKE, Google's own [documentation](https://cloud.google.com/kubernetes-engine/docs/quickstart) is an excellent starting point.

| Variable                           | Example Value       | Description                                        |
| ---------------------------------- | --------------------| ---------------------------------------------------|
| `ARGO_GCLOUD_PROJECT_NAME`         | `project-name`      | GCP project name.                                  |
| `ARGO_GCLOUD_STAGING_CLUSTER_NAME` | `example-cluster-1` | GKE cluster name.                                  |
| `ARGO_GCLOUD_STAGING_CLUSTER_ZONE` | `us-central2-a`     | GKE cluster zone.                                  |
| `ARGO_GCLOUD_PROD_CLUSTER_NAME`    | `example-cluster-1` | GKE cluster name.                                  |
| `ARGO_GCLOUD_PROD_CLUSTER_ZONE`    | `us-central2-a`     | GKE cluster zone.                                  |
| `ARGO_PRODUCTION_NAMESPACE`        | `default`           | Kubernetes namespace. Defaults to `default`.       |
| `ARGO_STAGING_NAMESPACE`           | `default`           | Kubernetes namespace.  Defaults to `default`.      |
| `ARGO_K8S_DEPLOY_TIMEOUT`          | `1800`              | Time in seconds to timeout. Default is 30 minutes. |
| `ARGO_GCLOUD_PROJECT_KEY_FILE`     | A JSON file.        | [gcloud IAM service-account keys](https://cloud.google.com/sdk/gcloud/reference/auth/activate-service-account) |

## Commands

Argo has the following commands available for use in your ci file.

### node_setup

`node_setup` will handle any private registry log in requirements and will run [npm ci](https://docs.npmjs.com/cli/ci.html) to automatically fetch dependencies if a `package-lock.json` is available.

### poetry_setup

`poetry_setup` will handle any private pip repository log in requirements and will run [poetry install](https://poetry.eustace.io/docs/cli/#install) to automatically fetch dependencies if a `pyproject.toml` is available.

### docker_setup

`docker_setup` will handle any private Docker registry log in requirements, so that you can build and publish images to a private location.

### deploy

`deploy` handles version bumping, in both a version file and git tags. Deploy will also attempt to write changelog information for you. For more information on the changelog entry details, check the [automated changelogs](#-automated-changelogs) section of this readme. For npm packages, `deploy` can also run any additional deployment and publish steps defined in your `package.json` with the `npm run deploy` keyword. For more information on `npm run` scripts, check [npm's official documentation](https://docs.npmjs.com/cli/run-script.html).

For more information on the flow that `deploy` uses, checkout [Itential's Git Flow](./GIT_FLOW.md).

For npm packages, Argo's `deploy` command will publish your module to a registry endpoint, as long as you have an npm run script named `deploy` in your `package.json`.

For example, adding the following to your project's `package.json` will publish to an example registry endpoint.

```json
"scripts": {
  "deploy": "npm publish --registry https://example.com/repositories/example/"
},
```

For maven projects, `deploy` requries a `pom.xml` with the version defined.

For [NCS packages](https://developer.cisco.com/docs/nso/), `deploy` requires a `package-meta-data.xml` with the version defined.

For Python packages, `deploy` requires a `pyproject.toml` with the version defined.

For any other project type, `deploy` requires a `VERSION` file in the root of the project with the version defined.

### prerelease

Aside from a few key differences, `prerelease` follows the same flow as `deploy`. `prerelease` assumes a `major` branch is being used. When pushing to the major branch, `prerelease` will create and deploy a new version that is ready to use, with a prerelease tag. For example, if your project is at version `1.2.3`, creating a `major` branch would create a new prerelease tag at version `2.0.0-0`. This allows for quick prototyping and testing before releasing a new major release.

### docs_transport

`docks_transport` allows users to specify a single repository to organize project documentation. This is accomplished by allowing users to specify a `/docs` directory in every project repository. During a pipeline run, the `/docs` directory is copied from the current running project pipeline to a predefined central repository (`$DOCS_PROJECT_NAME`). The content from the current running project pipeline replaces any existing content committed by that project. The updated content is then committed and available for publishing.

### docker_deploy

`docker_deploy` deploys your project as a Docker image. It also includes additional error checking to make sure a `Dockerfile` exists in the root of the project, and that a `$DOCKER_REGISTRY_URL` variable is set. `docker_deploy` will deploy a tag based on the triggered branch. If the branch is `master`, `docker_deploy` creates two tags. It creates a tag based off of the current semantic version as well as a `latest` tag and deploys them to the registry. If the branch is not `master`, `docker_deploy` assumes it is being run in a staging environment. It deploys a version tagged with the git short-hash of the current commit. After `docker_deploy` has committed the configuration change, it runs a clean up on the registry (*currently only supports hosted gitlab*) which removes images tagged with git-short-hashes older than the fifth oldest hash in the current branch, up to ten hashes at a time. `latest` then, by designation is the last version merged into `master`.

### kubernetes_setup

`kubernetes_setup` authenticates `kubectl` and `gcloud` via environment variables with the cluster described.

### kubernetes_deploy

`kubernetes_deploy`, when explicitly declared, executes only when the `ARGO_GCLOUD_PROJECT_NAME` environment variable and a project `manifest/` directory are present. `kubernetes_deploy` updates the manifest files based on the current branch. If the current branch is `master`, any previous tags or application names are removed (if merging from a non-master branch), the previous application is removed from the cluster, the most recent git tag is added to the image in the deployment, the names are restored to the original application name, the updates are committed, and the new configuration is pushed to kubernetes for rolling update of the production deployment. If the branch is not `master`, the application name is updated in the manifests to include the branch name, the image is tagged with the latest git short hash, the configuration is committed, and pushed to kubernetes for a non production deployment. A visual representation of this production/staging development process can be found [here](./src/img/master_non-master_flow.jpg).

## Automated Changelogs

Argo's `deploy` script will automatically handle changelog entries on every versioned release.

If you have the Jira environment variables configured, Argo will use the provided custom field's content as the changelog entry. The changelog entry will include the new version, the date, and a link to the related Jira ticket.

If you don't have the Jira environment variables configured, Argo will use the project's merge request subject as the changelog entry. The changelog entry will include the new version, the date, and a link to the related merge commit.

## Image Variants

The `argo` images come in various versions. All builds use the official `docker` (docker-in-docker) image as a baseline.

Python-based builds use [dind-python](https://gitlab.com/itentialopensource/dind-python).

Node.js-based builds use [dind-node](https://gitlab.com/itentialopensource/dind-node).

## Contributing to Argo

Please check out the [Contributing Guidelines](./CONTRIBUTING.md).

To build the image locally for testing, perform the following step.

```bash
# Create Docker images to test locally.
bash build.sh
```

Then you can run the images and play around with them.

```bash
# Start a container and start a shell session.
docker run -it itential/argo:latest bash -l
```
## License

[Apache 2.0](./LICENSE)
