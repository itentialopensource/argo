# Understanding Itential's Git Flow

![Itential-git-flow](./src/img/argo-flow.png)

Argo uses a slightly-modified github-based git workflow. This workflow is lightweight and branch-based to allow for easy development and regular deployments. The following guide will explain how Itential's git flow works.

## 1. Create a Branch

When working on a project, there will be many code changes and ideas in progress at any given time. Creating branches helps to manage the various ideas and code changes. Changes made on a new branch will not affect the `master` branch, allowing for local testing
 and experimentation with the proposed code changes. The main rule to remember is that the `master` and `release/*` branches, at any given moment, __should always be deployable__. It is extremely important to make sure that unfinished merge requests are not merged into the `master` branch.

#### For Patches:

```sh
git checkout master && git checkout -b patch/examplepatch

# using gitlab issue numbers. For example, if the issue number is 42...
git checkout master && git checkout -b patch/42

# using jira ticket numbers. For example, if the jira ticket is ABC-123
git checkout master && git checkout -b patch/ABC-123
```

#### For Minor Changes:

```sh
git checkout master && git checkout -b minor/exampleminor

# using gitlab issue numbers. For example, if the issue number is 42...
git checkout master && git checkout -b minor/42

# using jira ticket numbers. For example, if the jira ticket is ABC-123
git checkout master && git checkout -b minor/ABC-123
```

#### For Major Changes:

```sh
git checkout master && git checkout -b major/examplemajor

# using gitlab issue numbers. For example, if the issue number is 42...
git checkout master && git checkout -b major/42

# using jira ticket numbers. For example, if the jira ticket is ABC-123
git checkout master && git checkout -b major/ABC-123
```

### For Patching Releases:

First, consider making your change directly into the master branch first. Presumably whatever you are patching into the release should belong there as well.

Either way, when it comes time to make a patch to the release branch itself, you will follow the same process as above, except this time
1. The branch prefix must be "patch/". No minor or major changes allowed in a release branch.
2. The branch will be created *from the release branch*, and not from master branch.

```sh
git checkout relase/2019.1 && git checkout -b patch/examplepatch

# using gitlab issue numbers. For example, if the issue number is 42...
git checkout relase/2019.1 && git checkout -b patch/42

# using jira ticket numbers. For example, if the jira ticket is ABC-123
git checkout relase/2019.1 && git checkout -b patch/ABC-123
```


## 2. Add Commits

- `git commit -m "Add new check to ensure tasks in config object are always arrays"`

Creating a branch allows for changes to be made. To record and share these changes, meaningful commit messages are required. Commits help keep track of progress as a branch is worked on. Commits also create a transparent history of a developer's work that others can follow to understand what has been changed and why. Each commit has an associated commit message, which is a description explaining why a particular change was made. Furthermore, each commit is considered a separate unit of change. This lets developers roll back changes if a bug is found.

Commits are stored locally, so after creating commits, a developer can push their commits to a central repository (GitLab, Bitbucket, etc.) at any time. On every push, assuming the project has a pipeline set up, the project's ci pipelines will be triggered. The pipelines should run linting and testing steps every time, giving developers an easy way to ensure that their branch is passing/failing the project's coding standards.

Please read any style guidelines (if any) for the Argo-enabled repository you are working with.

### For release branches

Ideally, the fix you are adding to the release branch already exists inside master branch. Feel free to copy/paste those same changes into the release branch, however, if you truly are doing nothing more than just copy/pasting the same changes into the release branch, then consider using git's "cherry-pick" function:

If you know that a previous master merge request has the fix you want to apply to the release branch, first find the git commit id where your merge request was merged into master. Then you can do the following:

```sh
# This step assumes you already created the patch from release branch per above instructions
git checkout patch/examplepatch

# Assuming that the commit id you want to include is 'abcd1234'
# -m 1 ==> means that the source commit is a merge request, and you are want to apply the changes it introduced to the mainline ("1st parent" in git-speak)
# -x ==> means to add text to the commit message saying that it is a cherry-pick from abcd1234
git cherry-pick -x -m 1 abcd1234
```

Note: cherry-picking to add fixes to a release branch is not necessary. However, it removes the need to prove that you copy pasted everything correctly from the original fix added into master branch.

The cherry-pick command will, itself, add a new commit to your `release/examplepatch` branch.


## 3. Open a Merge Request (Pull Request)

Merge Requests (sometimes called Pull Requests) initiate a discussion and feedback loop about your branch and its proposed changes. Because they're tightly integrated with the underlying Git repository, anyone can see exactly what changes would be merged if they accept a merge request.

Developers are free to open a Merge Request at any point during the development process:

* when there is little or no code but the developer wants to share screenshots or general ideas
* when the progress of the branch is stuck and the developer needs help or advice
* when the branch is ready for review

Itential developers use gitlab's [@mention system in the discussions](https://docs.gitlab.com/ee/user/discussions/) (or in the Merge Request's post content) to notify or ask for feedback from specific people or groups, regardless of their physical location.

When creating a merge request, make sure to fill out the post template that is provided.

If the project uses GitLab issues, include the issue number(s) related to the merge request:

`Resolves !4`

If the project uses JIRA, use the JIRA ticket number:

`Resolves ABC-123`

For easier searching, include relevant comments, tags, and/or labels to the Merge Request so that reviewers can easily tell the merge request's current status.

## 4. Discuss & Review Code

Once a Merge Request has been opened, the person or group reviewing the changes may have questions, comments, or other advice. Perhaps the coding style doesn't match project guidelines, the change is missing unit tests, or maybe everything looks great and ready to ship. Merge Requests are designed to encourage this type of interaction between reviewers and submitters.

Just because a developer submitted a Merge Request doesn't mean that they're locked from pushing more changes to the same branch (to be added to the merge request). Instead, discussions and reviews should encourage developers to make changes based on the feedback that they receive. This should be an iterative process, allowing for a loop of feedback and fixes until the branch is ready to be accepted and merged in.

## 5. Merge

Once a merge request has been reviewed and approved, the branch with the code changes is ready to be merged into the target branch (should always be master with very few exceptions).

Once merged, Merge Requests preserve a record of the historical changes to the codebase. Because they're searchable, they afford the ability to go back in time to understand why and how a decision was made.
